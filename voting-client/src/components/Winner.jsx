import React from 'react';
import PureRenderMixim from 'react-addons-pure-render-mixin';

export default React.createClass( {
	mixins: [PureRenderMixim],
	render: function() {
		return <div className="winner">
			Winner is {this.props.winner}!
		</div>;
	}
} )